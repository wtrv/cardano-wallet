<?php
/** @noinspection PhpUnusedPrivateFieldInspection */
declare(strict_types=1);

namespace CardanoWallet\API\Wallets;

use CardanoWallet\API\RawTransaction;
use CardanoWallet\Cardano;
use CardanoWallet\Exception\API_ResponseException;
use CardanoWallet\Exception\WalletException;
use CardanoWallet\Response\Transaction;
use CardanoWallet\Response\TransactionsList;
use CardanoWallet\Response\WalletInfo;
use CardanoWallet\Validate;
use furqansiddiqui\BIP39\Mnemonic;
use GuzzleHttp\RequestOptions;

/**
 * Class Wallet
 * @package CardanoWallet\API\Wallet
 * @property-read null|string $spendingPassword
 * @property-read string $id
 * @property-read bool $hasInfoLoaded
 */
class Wallet
{
    /** @var Cardano */
    private Cardano $node;
    /** @var string */
    private string $id;
    /** @var null|WalletInfo */
    private ?WalletInfo $info = null;
    /** @var null|Mnemonic */
    private ?Mnemonic $mnemonic = null;
    /** @var null|string */
    private ?string $spendingPassword = null;

    /** @var null|bool */
    private ?bool $_isDeleted = null;

    /**
     * Wallet constructor.
     * @param Cardano $node
     * @param string $id
     * @param WalletInfo|null $walletInfo
     * @param Mnemonic|null $mnemonic
     * @throws WalletException
     */
    public function __construct(Cardano $node, string $id, ?WalletInfo $walletInfo = null, ?Mnemonic $mnemonic = null)
    {
        $this->node = $node;
        $this->id = $id;

        if ($walletInfo) {
            $this->id = $walletInfo->id;
            $this->info = $walletInfo;
            $this->mnemonic = $mnemonic;
        }

        self::isValidIdentifier($this->id);
    }

    /**
     * @return array
     */
    public function __debugInfo()
    {
        return [sprintf('Cardano wallet "%s" API instance', $this->id)];
    }

    /**
     * @param string $prop
     * @return string|null
     * @throws WalletException
     */
    public function __get(string $prop)
    {
        switch ($prop) {
            case "spendingPassword":
                return $this->spendingPassword;
            case "id":
                return $this->id;
            case "hasInfoLoaded":
                return isset($this->info);
        }

        throw new WalletException(sprintf('Cannot access unreadable prop "%s"', $prop));
    }

    /**
     * @return $this
     * @throws API_ResponseException
     * @throws WalletException
     */
    public function load(): self
    {
        $this->info();
        return $this;
    }

    /**
     * @param bool $forceReload
     * @return WalletInfo
     * @throws WalletException
     */
    public function info(bool $forceReload = false): WalletInfo
    {
        $this->isWalletDeleted();

        if ($this->info && !$forceReload) {
            return $this->info;
        }

        $response = $this->node->getClient()->get(sprintf('wallets/%s', $this->id));
        $this->info = new WalletInfo($this->node->responseToArray($response));
        return $this->info;
    }

    /**
     * @return array
     */
    public function getAllAddresses(): array
    {
        $addresses = $this->node->getClient()->get(sprintf('wallets/%s/addresses', $this->id));
        return $this->node->responseToArray($addresses);
    }

    /**
     * @param string $walletName
     * @return WalletInfo
     * @throws API_ResponseException
     */
    public function update(string $walletName): WalletInfo
    {
        $this->isWalletDeleted();

        if (!Validate::WalletName($walletName)) {
            throw API_ResponseException::InvalidPropValue('walletName');
        }

        $payload = [
            "name" => $walletName
        ];

        $update = $this->node->getClient()->put(sprintf('wallets/%s', $this->id), [
            RequestOptions::JSON => $payload,
        ]);
        $this->info = new WalletInfo($this->node->responseToArray($update));

        return $this->info;
    }

    /**
     * @throws WalletException
     */
    public function delete(): bool
    {
        $this->isWalletDeleted();

        $response = $this->node->getClient()->delete(sprintf('wallets/%s', $this->id));
        $this->_isDeleted = $response->getStatusCode() === 204;

        return $this->_isDeleted;
    }

    /**
     * @param string $newPassword
     * @param string $oldPassword
     * @param bool $hashPasswords
     * @return bool
     * @throws WalletException
     */
    public function changePassword(string $newPassword, string $oldPassword, bool $hashPasswords = true): bool
    {
        $this->isWalletDeleted();

        $encodedNewPassword = $hashPasswords ? hash("sha256", $newPassword) : $newPassword;
        if (!Validate::Hash64($encodedNewPassword)) {
            //throw new WalletException('newPassword must be 32 byte hexadecimal string (64 hexits)');
        }

        $encodedOldPassword = "";
        if ($oldPassword) { // If no password is passed, just send an empty string ""
            if ($hashPasswords) {
                $encodedOldPassword = hash("sha256", $oldPassword);
            }

            if (!Validate::Hash64($encodedOldPassword)) {
                //throw new WalletException('oldPassword must be 32 byte hexadecimal string (64 hexits)');
            }
        }

        $payload = [
            "new_passphrase" => $encodedNewPassword,
            "old_passphrase" => $encodedOldPassword
        ];

        $response = $this->node->getClient()->put(sprintf('wallets/%s/passphrase', $this->id), [
            RequestOptions::JSON => $payload,
        ]);
        return $response->getStatusCode() === 204;
    }

    /**
     * ONLY return Mnemonic object for instances result of create/restore operations
     * @return null|Mnemonic
     */
    public function mnemonic(): ?Mnemonic
    {
        return $this->mnemonic;
    }

    /**
     * @param string|null $start
     * @param string|null $end
     * @param string|null $order
     * @param int|null $minWithdrawal
     * @return TransactionsList
     */
    public function getTransactions(?string $start = null, ?string $end = null, ?string $order = null, ?int $minWithdrawal = null): TransactionsList
    {
        $payload = [];

        if ($start) {
            $payload["start"] = $start;
            if ($end) {
                $payload["end"] = $end;
            }
        }

        if ($order) {
            if (!in_array($order, ["ascending", "descending"])) {
                throw new \InvalidArgumentException('Invalid value for order argument; Enum "ascending" or "descending" accepted');
            }

            $payload["order"] = $order;
        }

        if (is_int($minWithdrawal) && $minWithdrawal >= 1) {
            $payload["minWithdrawal"] = $minWithdrawal;
        }

        $response = $this->node->getClient()->get(sprintf('wallets/%s/transactions', $this->id), [
            RequestOptions::QUERY => $payload,
        ]);
        return new TransactionsList($this->node->responseToArray($response));
    }

    /**
     * @param string $txId
     * @return Transaction
     */
    public function getTransaction(string $txId): Transaction
    {
        if (!preg_match('/^[a-f0-9]{64}$/i', $txId)) {
            throw new \InvalidArgumentException('Invalid transaction ID/hash');
        }

        $response = $this->node->getClient()->get(sprintf('wallets/%s/transactions/%s', $this->id, $txId));
        return new Transaction($this->node->responseToArray($response));
    }

    /**
     * @param string $password
     * @param bool $hashPassword
     * @return Wallet
     * @throws WalletException
     */
    public function spendingPassword(string $password, bool $hashPassword = true): self
    {
        $this->isWalletDeleted();

        $encodedPassword = $hashPassword ? hash("sha256", $password) : $password;
        if (!Validate::Hash64($encodedPassword)) {
            //throw new WalletException('spendingPassword must be 32 byte hexadecimal string (64 hexits)');
        }

        $this->spendingPassword = $encodedPassword;
        return $this;
    }

    /**
     * @param RawTransaction $tx
     * @return Transaction
     */
    public function sendTx(RawTransaction $tx): Transaction
    {
        $this->isWalletDeleted();

        if (!$this->spendingPassword) {
            throw new WalletException('Wallet spending passphrase is not set');
        }

        $outputs = $tx->getOutputs();
        if (!$outputs) {
            throw new WalletException('Cannot send empty transaction; No outputs defined');
        }

        $response = $this->node->getClient()->post(sprintf('wallets/%s/transactions', $this->id), [
            RequestOptions::JSON => [
                "passphrase" => $this->spendingPassword,
                "payments" => $outputs,
            ],
        ]);

        return new Transaction($this->node->responseToArray($response));
    }

    /**
     * @param RawTransaction $tx
     * @return array
     */
    public function estimateFee(RawTransaction $tx): array
    {
        $this->isWalletDeleted();

        $outputs = $tx->getOutputs();
        if (!$outputs) {
            throw new WalletException('Cannot send empty transaction; No outputs defined');
        }

        $response = $this->node->getClient()->post(sprintf('wallets/%s/payment-fees', $this->id), [
            RequestOptions::JSON => [
                "payments" => $outputs,
            ],
        ]);

        return $this->node->responseToArray($response);
    }

    /**
     * @throws WalletException
     */
    private function isWalletDeleted(): void
    {
        if ($this->_isDeleted) {
            throw new WalletException(sprintf('Wallet "%s" is deleted, cannot perform requested op', $this->id));
        }
    }

    /**
     * @param $walletId
     * @throws WalletException
     */
    public static function isValidIdentifier($walletId): void
    {
        if (!Validate::WalletIdentifier($walletId)) {
            throw new WalletException('Invalid wallet identifier');
        }
    }
}
