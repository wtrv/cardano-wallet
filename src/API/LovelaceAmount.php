<?php
declare(strict_types=1);

namespace CardanoWallet\API;

use CardanoWallet\Cardano;
use CardanoWallet\Exception\AmountException;
use CardanoWallet\Response\LovelaceAmount as LovelaceAmountReponse;
use CardanoWallet\Validate;

/**
 * Class LovelaceAmount
 * @package CardanoWallet\API
 */
class LovelaceAmount extends LovelaceAmountReponse
{
    /**
     * LovelaceAmount constructor.
     * @param int $lovelace
     */
    public function __construct(int $lovelace = 0)
    {
        parent::__construct(["unit" => "lovelace", "quantity" => $lovelace]);
    }

    /**
     * @param string $amount
     * @return static
     * @throws AmountException
     */
    public static function ADA(string $amount): self
    {
        if (!Validate::BcAmount($amount, Cardano::SCALE, false)) {
            throw new AmountException('Invalid ADA amount');
        }

        $lovelaceAmount = new self();
        $lovelaceAmount->ada = $amount;
        $lovelaceAmount->lovelace = intval(bcmul($amount, bcpow("10", strval(Cardano::SCALE), 0), Cardano::SCALE));
        return $lovelaceAmount;
    }

    /**
     * @param int $amount
     * @return static
     * @throws AmountException
     */
    public static function Lovelace(int $amount): self
    {
        return new self($amount);
    }
}
