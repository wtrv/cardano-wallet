<?php
declare(strict_types=1);

namespace CardanoWallet\Exception;

/**
 * Class TransactionException
 * @package CardanoWallet\Exception
 */
class TransactionException extends CardanoException
{
}
