<?php
declare(strict_types=1);

namespace CardanoWallet\Exception;

/**
 * Class WalletException
 * @package CardanoWallet\Exception
 */
class WalletException extends API_Exception
{
}
