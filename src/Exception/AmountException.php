<?php
declare(strict_types=1);

namespace CardanoWallet\Exception;

/**
 * Class AmountException
 * @package CardanoWallet\Exception
 */
class AmountException extends WalletException
{
}
