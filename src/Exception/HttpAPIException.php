<?php
declare(strict_types=1);

namespace CardanoWallet\Exception;

/**
 * Class HttpAPIException
 * @package CardanoWallet\Exception
 */
class HttpAPIException extends CardanoException
{
}
