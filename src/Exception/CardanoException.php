<?php
declare(strict_types=1);

namespace CardanoWallet\Exception;

/**
 * Class CardanoException
 * @package CardanoWallet\Exception
 */
class CardanoException extends \Exception
{
}
