<?php
declare(strict_types=1);

namespace CardanoWallet\Response;

/**
 * Class WalletAssets
 * @package CardanoWallet\Response
 */
class WalletAssets
{
    /** @var array */
    public array $available = [];
    /** @var array */
    public array $total = [];
}
