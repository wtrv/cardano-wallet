<?php
declare(strict_types=1);

namespace CardanoWallet\Response;

/**
 * Class WalletBalance
 * @package CardanoWallet\Response
 */
class WalletBalance
{
    /** @var LovelaceAmount|null */
    public ?LovelaceAmount $available = null;
    /** @var LovelaceAmount|null */
    public ?LovelaceAmount $reward = null;
    /** @var LovelaceAmount|null */
    public ?LovelaceAmount $total = null;
}
