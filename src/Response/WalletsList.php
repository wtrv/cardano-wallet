<?php
declare(strict_types=1);

namespace CardanoWallet\Response;

/**
 * Class WalletsList
 * @package CardanoWallet\Response
 */
class WalletsList implements \Iterator, \Countable, ResponseModelInterface
{
    private int $pos = 0;
    private int $count = 0;
    private array $wallets = [];

    /**
     * WalletsList constructor.
     */
    public function __construct(array $data)
    {
        foreach ($data as $wallet) {
            $this->wallets[] = new WalletInfo($wallet);
            $this->count++;
        }
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return $this->wallets;
    }

    /**
     * @return void
     */
    public function rewind(): void
    {
        $this->pos = 0;
    }

    /**
     * @return WalletInfo
     */
    public function current(): WalletInfo
    {
        return $this->wallets[$this->pos];
    }

    /**
     * @return int
     */
    public function key(): int
    {
        return $this->pos;
    }

    /**
     * @return void
     */
    public function next(): void
    {
        ++$this->pos;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->wallets[$this->pos]);
    }
}
