<?php
declare(strict_types=1);

namespace CardanoWallet;

use CardanoWallet\API\Wallets;
use CardanoWallet\Exception\API_Exception;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Cardano
 */
class Cardano
{
    public const SCALE = 6;
    public const MAX_LOVELACE = 45000000000000000;
    public const MIN_ACCOUNTS_INDEX = 2147483648;
    public const MAX_ACCOUNTS_INDEX = 4294967295;

    private ?Wallets $wallets = null;
    private ClientInterface $client;

    /**
     * Cardano constructor.
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return Wallets
     */
    public function wallets(): Wallets
    {
        if ($this->wallets instanceof Wallets) {
            return $this->wallets;
        }

        $this->wallets = new Wallets($this);
        return $this->wallets;
    }

    /**
     * @param string $addr
     * @return array
     * @throws API_Exception
     */
    public function getAddress(string $addr): array
    {
        if (!Validate::Address($addr)) {
            throw new API_Exception('Invalid address in argument for getAddress');
        }

        $response = $this->client->get('addresses/' . $addr);
        return $this->responseToArray($response);
    }

    /**
     * @return array
     * @throws API_Exception
     */
    public function nodeInfo(): array
    {
        $response = $this->client->get('network/information');
        return $this->responseToArray($response);
    }

    /**
     * @return ClientInterface
     */
    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     */
    public function responseToArray(ResponseInterface $response): array
    {
        return json_decode($response->getBody()?->getContents(), TRUE);
    }
}
